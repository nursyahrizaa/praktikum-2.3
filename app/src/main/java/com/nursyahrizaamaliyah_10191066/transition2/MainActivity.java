package com.nursyahrizaamaliyah_10191066.transition2;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    button.setOnClickListener(new View.OnClickListener) static {

        @Override
                Public void onClick(View v) {
            if(!turnOn) {
                imageview.setImageResource(R.drawable.trans_on);
                ((TransitionDrawable) imageview.getDrawable()).startTransition(3000);
                turnOn = true;
            }else {
                imageview.setImageResource(R.drawable.trans_off);
                ((TransitionDrawable) imageview.getDrawable()).startTransition(3000);
                turnOn = false;
            }
        }
    }
}